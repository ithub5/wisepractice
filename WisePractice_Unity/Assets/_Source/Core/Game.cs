using System.Reflection;
using GameUI.TowerSystem;
using InputSystem;
using TowersSystem.Building;
using TowersSystem.Data.Building;

namespace Core
{
    public class Game
    {
        private InputHandler _input;

        private BuildUI _buildUI;

        public Game(InputHandler input, BuildUI buildUI)
        {
            _input = input;

            _buildUI = buildUI;
            
            Start();
        }

        private void Start()
        {
            _input.Enable();
            Bind();
        }

        private void Bind()
        {
            _buildUI.Bind();
        }
    }
}