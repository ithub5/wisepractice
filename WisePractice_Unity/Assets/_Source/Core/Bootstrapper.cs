using System;
using System.Collections;
using System.Collections.Generic;
using GameUI.TowerSystem;
using InputSystem;
using TowersSystem.Building;
using TowersSystem.Data.Building;
using TowersSystem.Shooting;
using UnityEngine;

namespace Core
{
    public class Bootstrapper : MonoBehaviour
    {
        [SerializeField] private Camera cam;

        [Header("Builder")] 
        [SerializeField] private Transform projectilesHolder;

        [SerializeField] private BuildUI buildUI;
        [SerializeField] private InfoNUpgradeUI infoNUpgradeUI;
        
        private Game _game;

        private InputHandler _input;

        private Builder _builder;

        private void Awake()
        {
            _input = new InputHandler();
            _builder = new Builder(_input, cam, projectilesHolder, infoNUpgradeUI);
            
            buildUI.Init(_builder);
            
            _game = new Game(_input, buildUI);
        }
    }
}