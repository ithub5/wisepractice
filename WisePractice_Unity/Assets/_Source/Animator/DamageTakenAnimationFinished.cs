using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTakenAnimationFinished : StateMachineBehaviour
{
    private static readonly int Damaged = Animator.StringToHash("Damaged");
    
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool(Damaged,false);
    }
}
