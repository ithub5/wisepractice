using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerResources
{
    public static event Action OnEnergyChange;
    public static float Energy { get; private set; }

    public static void ChangeEnergy(float value)
    {
        Energy += value;
        OnEnergyChange?.Invoke();
    }

    public static void ResetEnergy()
    {
        Energy = 0;
        OnEnergyChange?.Invoke();
    }
}
