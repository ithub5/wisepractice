using TMPro;
using TowersSystem.Core;
using UnityEngine;
using UnityEngine.UI;

namespace GameUI.TowerSystem
{
    public class InfoNUpgradeUI : MonoBehaviour
    {
        [Header("Main")]
        [SerializeField] private GameObject infoPanel;
        [SerializeField] private TextMeshProUGUI infoText;
        
        [Header("Buttons")]
        [SerializeField] private Button upgradeButton;
        [SerializeField] private TextMeshProUGUI upgradeText;

        [SerializeField] private Button closeButton;

        [Header("Info Text")]
        [SerializeField] private string damageText;
        [SerializeField] private string projectileTravelSpeedText;
        [SerializeField] private string shootDelayText;

        private TowerUpgrader _currentUpgrader;
        private TowerInvoker _currentInvoker;

        private void Start()
        {
            upgradeButton.onClick.AddListener(Upgrade);
            closeButton.onClick.AddListener(HideInfo);
        }

        public void SetTarget(TowerUpgrader towerUpgrader, TowerInvoker towerInvoker)
        {
            _currentUpgrader = towerUpgrader;
            _currentInvoker = towerInvoker;
            ShowInfo();
        }

        private void ShowInfo()
        {
            if (!infoPanel.activeSelf)
            {
                PlayerResources.OnEnergyChange += CheckEnergy;
                CheckEnergy();
                infoPanel.SetActive(true);
            }

            ShowStats();
            ShowCost();
        }

        private void Upgrade()
        {
            _currentUpgrader.Upgrade();
            ShowInfo();
        }

        private void HideInfo()
        {
            _currentInvoker.HideInfo();
            PlayerResources.OnEnergyChange -= CheckEnergy;
            infoPanel.SetActive(false);
        }
        
        private void ShowStats()
        {
            infoText.text = damageText + _currentUpgrader.Damage + "\n";
            infoText.text += projectileTravelSpeedText + _currentUpgrader.ProjectileTravelVelocity + "\n";
            infoText.text += shootDelayText + _currentUpgrader.ShootDelay;
        }

        private void ShowCost()
        {
            int nextLevelCost = _currentUpgrader.Cost;
            if (nextLevelCost > 0)
            {
                upgradeText.text = nextLevelCost.ToString();
            }
            else
            {
                upgradeText.text = "MAX";
                upgradeButton.interactable = false;
            }
        }

        private void CheckEnergy()
        {
            bool hasEnoughEnergy = _currentUpgrader.Cost > 0 && PlayerResources.Energy >= _currentUpgrader.Cost;
            if (upgradeButton.interactable != hasEnoughEnergy)
            {
                upgradeButton.interactable = hasEnoughEnergy;
            }
        }
    }
}