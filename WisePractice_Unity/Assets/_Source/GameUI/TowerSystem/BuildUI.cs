using System.Collections.Generic;
using TMPro;
using TowersSystem.Building;
using TowersSystem.Core;
using TowersSystem.Data.Building;
using UnityEngine;

namespace GameUI.TowerSystem
{
    public class BuildUI : MonoBehaviour
    {
        [SerializeField] private TowerNButton[] buttonNTowers;

        private List<int> prices = new List<int>();

        private Builder _builder;

        private void Start()
        {
            for (int i = 0; i < buttonNTowers.Length; i++)
            {
                TextMeshProUGUI priceText = buttonNTowers[i].Button.transform.GetComponentInChildren<TextMeshProUGUI>();
                int price = buttonNTowers[i].TowerPrefab.GetComponent<TowerUpgrader>().Cost;
                prices.Add(price);

                priceText.text = price.ToString();
                CheckEnergy();
            }
        }

        private void OnDisable()
        {
            PlayerResources.OnEnergyChange -= CheckEnergy;
        }

        public void Init(Builder builder)
        {
            _builder = builder;
        }
        
        public void Bind()
        {
            for (int i = 0; i < buttonNTowers.Length; i++)
            {
                int tempI = i;
                buttonNTowers[i].Button.onClick.AddListener(
                    () => _builder.BuildTower(buttonNTowers[tempI].TowerPrefab));
            }

            PlayerResources.OnEnergyChange += CheckEnergy;
        }

        private void CheckEnergy()
        {
            for (int i = 0; i < prices.Count; i++)
            {
                bool hasEnoughEnergy = PlayerResources.Energy >= prices[i];
                if (buttonNTowers[i].Button.interactable != hasEnoughEnergy)
                {
                    buttonNTowers[i].Button.interactable = PlayerResources.Energy >= prices[i];
                }
            }
        }
    }
}