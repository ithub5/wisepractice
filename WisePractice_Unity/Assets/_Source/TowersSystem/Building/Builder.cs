using System.Collections.Generic;
using Core;
using GameUI.TowerSystem;
using InputSystem;
using TowersSystem.Core;
using TowersSystem.Data.Building;
using UnityEngine;
using UnityEngine.InputSystem;

namespace TowersSystem.Building
{
    public class Builder
    {
        private InputHandler _input;
        
        private Dictionary<GameObject, Queue<TowerNInvoker>> _instantiatedPrefabs;

        private Camera _cam;
        private Transform _projectilesHolder;

        private InfoNUpgradeUI _infoNUpgradeUI;

        public GameObject CurrentPrefab { get; private set; }
        public TowerNInvoker CurrentInstance { get; private set; }

        public Builder(InputHandler input, Camera cam, Transform projectilesHolder, InfoNUpgradeUI infoNUpgradeUI)
        {
            _instantiatedPrefabs = new Dictionary<GameObject, Queue<TowerNInvoker>>();

            _input = input;
            _cam = cam;
            _projectilesHolder = projectilesHolder;

            _infoNUpgradeUI = infoNUpgradeUI;
        }

        public void Bind()
        {
            _input.TowerPlacementActions.Interaction.performed += Position;
            _input.TowerPlacementActions.AlterInteration.performed += Cancel;
            _input.TowerPlacementActions.MouseDelta.performed += MoveTower;
        }

        public void Expose()
        {
            _input.TowerPlacementActions.Interaction.performed -= Position;
            _input.TowerPlacementActions.AlterInteration.performed -= Cancel;
            _input.TowerPlacementActions.MouseDelta.performed -= MoveTower;
        }
        
        public void BuildTower(GameObject prefab)
        {
            if (CurrentInstance != null)
            {
                Cancel();
            }

            if (_instantiatedPrefabs.ContainsKey(prefab) &&
                _instantiatedPrefabs[prefab].Count > 0)
            {
                SetCurrents(prefab, _instantiatedPrefabs[prefab].Dequeue());
                CurrentInstance.Tower.SetActive(true);
            }
            else
            {
                SetCurrents(prefab, InitNewTower(prefab));
            }
            
            MoveTower();
            Bind();
        }

        private void MoveTower(InputAction.CallbackContext ctx = default)
        {
            Vector2 mousePos = _cam.ScreenToWorldPoint(Input.mousePosition);

            CurrentInstance.Tower.transform.position = mousePos;
            
            CurrentInstance.Invoker.UpdatePlacing();
        }

        private void Position(InputAction.CallbackContext ctx = default)
        {
            if (!CurrentInstance.Invoker.IsPlaceable)
            {
                return;
            }

            Vector2 mousePos = _cam.ScreenToWorldPoint(Input.mousePosition);

            CurrentInstance.Tower.transform.position = mousePos;
           
            Expose();
            
            CurrentInstance.Invoker.StopPlacing();
            SetCurrents(null, null);
        }

        private void Cancel(InputAction.CallbackContext ctx = default)
        {
            if (!_instantiatedPrefabs.ContainsKey(CurrentPrefab))
            {
                _instantiatedPrefabs.Add(CurrentPrefab, new Queue<TowerNInvoker>());
            }
            
            _instantiatedPrefabs[CurrentPrefab].Enqueue(CurrentInstance);

            Expose();
            
            CurrentInstance.Tower.SetActive(false);
            SetCurrents(null, null);
        }
        
        private void SetCurrents(GameObject prefab, TowerNInvoker instance)
        {
            CurrentPrefab = prefab;
            CurrentInstance = instance;
        }

        private TowerNInvoker InitNewTower(GameObject prefab)
        {
            GameObject newTower = Object.Instantiate(prefab);

            TowerUpgrader upgrader = newTower.GetComponent<TowerUpgrader>();
            TowerPhysics physics = newTower.GetComponent<TowerPhysics>();
            TowerView view = newTower.GetComponent<TowerView>();
            TowerShooter shooter = new TowerShooter(physics.FirePoint);
            TowerInvoker invoker = new TowerInvoker(upgrader, physics, view, shooter, _projectilesHolder);
            invoker.Bind();
            upgrader.Upgrade();
            invoker.OnShowInfo += _infoNUpgradeUI.SetTarget;
            
            invoker.StartPlacing();
            return new TowerNInvoker(newTower, invoker);
        }
    }
}