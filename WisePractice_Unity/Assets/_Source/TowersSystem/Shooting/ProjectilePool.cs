using System.Collections.Generic;
using UnityEngine;

namespace TowersSystem.Shooting
{
    public class ProjectilePool
    {
        private Transform _projectilesParent;
        
        private Queue<Projectile> _pool;

        public GameObject ProjectilePrefab { get; private set; }

        public ProjectilePool(Transform tower,GameObject projectilePrefab, int poolSize)
        {
            _projectilesParent = tower;
            ProjectilePrefab = projectilePrefab;
            
            InitPool(poolSize);
        }

        public Projectile Get()
        {
            return _pool.Dequeue();
        }

        private void Return(Projectile projectile)
        {
            _pool.Enqueue(projectile);
        }

        private void InitPool(int poolSize)
        {
            _pool = new Queue<Projectile>();
            
            for (int i = 0; i < poolSize; i++)
            {
                Projectile newProjectile = Object.Instantiate(ProjectilePrefab, _projectilesParent)
                    .GetComponent<Projectile>();
                newProjectile.OnEnemyHit += Return;
                newProjectile.gameObject.SetActive(false);
                _pool.Enqueue(newProjectile);
            }
        }
    }
}