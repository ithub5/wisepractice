using System;
using Entities;
using TowersSystem.Data.Stats.Specials;
using UnityEngine;

namespace TowersSystem.Shooting
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D rb;
        [SerializeField] private Collider2D mainCollider;
        
        private Enemy _target;
        private Collider2D _targetCollider;

        private TowerSpecialSO _specialSO;

        private float _damage;
        private float _travelVelocity;

        private bool _isShooting;

        public event Action<Projectile> OnEnemyHit;

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col == _targetCollider)
            {
                Hit();
            }
        }

        private void FixedUpdate()
        {
            AutoTarget();
        }

        public void ShootSelf(Enemy target, float damage, float travelVelocity, TowerSpecialSO specialSO)
        {
            _target = target;
            _target.OnDeath += ReturnToPool;

            _targetCollider = _target.GetComponent<Collider2D>();
            _specialSO = specialSO;
            gameObject.SetActive(true);

            _damage = damage;
            _travelVelocity = travelVelocity;
            _isShooting = true;
        }

        private void AutoTarget()
        {
            if (!_isShooting)
            {
                return;
            }
            
            transform.up = _target.transform.position - transform.position;

            rb.velocity = transform.up * _travelVelocity;
        }

        private void Hit()
        {
            _target.TakeDamage(_damage);
            SetSpecial();
            
            ReturnToPool();
        }
        
        private void SetSpecial()
        {
            if (_specialSO == null)
            {
                return;
            }

            if (_specialSO is TowerPoisonSO poisonSO)
            {
                _target.ApplyPoison(poisonSO);
            }
            else if (_specialSO is TowerDebuffSO debuffSO)
            {
                _target.ApplyDebuff(debuffSO);
            }
        }

        private void ReturnToPool(Enemy _ = default)
        {
            _isShooting = false;
            _target.OnDeath -= ReturnToPool;
            gameObject.SetActive(false);
            OnEnemyHit?.Invoke(this);
        }
    }
}