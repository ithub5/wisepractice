using System.Collections.Generic;
using System.Linq;
using TowersSystem.Shooting;
using UnityEngine;

namespace TowersSystem.Shooting
{
    public static class ProjectilePoolsSaver
    {
        private static List<ProjectilePool> _projectilePools;

        static ProjectilePoolsSaver()
        {
            _projectilePools = new List<ProjectilePool>();
        }
        
        public static void SavePool(ProjectilePool newPool)
        {
            _projectilePools.Add(newPool);
        }

        public static bool GetPool(GameObject projectileToLookFor, out ProjectilePool pool)
        {
            pool = _projectilePools.FirstOrDefault(pool => pool.ProjectilePrefab == projectileToLookFor);
            if (pool == null)
            {
                return false;
            }

            _projectilePools.Remove(pool);
            return true;
        }
    }
}