using TowersSystem.Data.Stats.Specials;
using UnityEngine;

namespace TowersSystem.Data.Stats
{
    [CreateAssetMenu(fileName = "NewTowerLevel", menuName = "SO/TowerSystem/Stats/TowerLevel")]
    public class TowerLevelSO : ScriptableObject
    {
        [field: SerializeField] public TowerGeneralStats GeneralStats { get; private set; }
        
        [field: SerializeField] public TowerSpecialSO TowerSpecial { get; private set; } 
    }
}