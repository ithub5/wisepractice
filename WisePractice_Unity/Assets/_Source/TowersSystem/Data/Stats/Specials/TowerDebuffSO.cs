using TowersSystem.Utils;
using UnityEngine;

namespace TowersSystem.Data.Stats.Specials
{
    [CreateAssetMenu(fileName = "NewTowerDebuff", menuName = "SO/TowerSystem/Stats/Specials/Debuff")]
    public class TowerDebuffSO : TowerSpecialSO
    {
        [field: SerializeField] public DebuffType Type { get; private set; }
        [field: SerializeField] public float Duration { get; private set; }
        [field: SerializeField] public float Value { get; private set; }
        [field: SerializeField] public Color Color { get; private set; }
    }
}