using UnityEngine;

namespace TowersSystem.Data.Stats.Specials
{
    [CreateAssetMenu(fileName = "NewTowerPoison", menuName = "SO/TowerSystem/Stats/Specials/Poison")]
    public class TowerPoisonSO : TowerSpecialSO
    {
        [field: SerializeField] public float Duration { get; private set; }
        [field: SerializeField] public float CycleTime { get; private set; }
        [field: SerializeField] public float Damage { get; private set; }
        [field: SerializeField] public Color Color { get; private set; }
    }
}