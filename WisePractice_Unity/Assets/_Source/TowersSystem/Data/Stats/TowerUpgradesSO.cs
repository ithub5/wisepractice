using System.Collections.Generic;
using UnityEngine;

namespace TowersSystem.Data.Stats
{
    [CreateAssetMenu(fileName = "NewTowerUpgrades", menuName = "SO/TowerSystem/Stats/TowerUpgrades")]
    public class TowerUpgradesSO : ScriptableObject
    {
        [field: SerializeField] public TowerLevelSO[] TowerLevels { get; private set; }
        [field: SerializeField] public int[] Prices { get; private set; }
    }
}