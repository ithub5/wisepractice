using TowersSystem.Data.Shooting;
using TowersSystem.Data.Stats.Specials;
using UnityEngine;

namespace TowersSystem.Data.Stats
{
    [System.Serializable]
    public class TowerGeneralStats
    {
        [field: SerializeField] public float ShootDelay { get; private set; }
        [field: SerializeField] public float ShootRadius { get; private set; }
        [field: SerializeField] public LayerMask EnemyMaks { get; private set; }
        [field: SerializeField] public LayerMask ObstaclesMask { get; private set; }
        
        [field: SerializeField] public ProjectileStats Projectile { get; private set; }
    }
}