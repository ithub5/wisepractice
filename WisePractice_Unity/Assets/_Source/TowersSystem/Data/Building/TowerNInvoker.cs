using TowersSystem.Core;
using UnityEngine;

namespace TowersSystem.Data.Building
{
    public class TowerNInvoker
    {
        [field: SerializeField] public GameObject Tower { get; private set; }
        [field: SerializeField] public TowerInvoker Invoker { get; private set; }

        public TowerNInvoker(GameObject tower, TowerInvoker invoker)
        {
            Tower = tower;
            Invoker = invoker;
        }
    }
}