using UnityEngine;
using UnityEngine.UI;

namespace TowersSystem.Data.Building
{
    [System.Serializable]
    public class TowerNButton
    {
        [field: SerializeField] public GameObject TowerPrefab { get; private set; }
        [field: SerializeField] public Button Button { get; private set; }
    }
}