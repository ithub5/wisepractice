using UnityEngine;

namespace TowersSystem.Data.Shooting
{
    [System.Serializable]
    public class ProjectileStats
    {
        [field: SerializeField] public float Damage { get; private set; }
        [field: SerializeField] public float TravelVelocity { get; private set; }
        
        [field: SerializeField] public bool UseSameProjectile { get; private set; }
        [field: SerializeField] public GameObject ProjectilePrefab { get; private set; }
    }
}