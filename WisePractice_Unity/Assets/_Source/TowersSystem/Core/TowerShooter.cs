using Audio;
using DG.Tweening;
using Entities;
using TowersSystem.Data.Shooting;
using TowersSystem.Data.Stats.Specials;
using TowersSystem.Shooting;
using UnityEngine;

namespace TowersSystem.Core
{
    public class TowerShooter
    {
        private float _damage;
        private float _travelVelocity;
        private float _shootDelay;

        private Transform _firePoint;
        private ProjectilePool _projectilePool;
        
        private Enemy _target;

        private Sequence _shootSequence;

        private TowerSpecialSO _specialSO;

        public TowerShooter(Transform firePoint)
        {
            _firePoint = firePoint;
        }
        
        public void Upgrade(Transform projectilesHolder, ProjectileStats stats, float shootDelay, TowerSpecialSO specialSO)
        {
            UpgradeStats(stats.Damage, stats.TravelVelocity, shootDelay, specialSO);
            
            if (stats.UseSameProjectile)
            {
                return;
            }

            ProjectilePool newPool;
            if (!ProjectilePoolsSaver.GetPool(stats.ProjectilePrefab, out newPool))
            {
                newPool = new ProjectilePool(projectilesHolder, stats.ProjectilePrefab, 12);
            }

            ProjectilePool previousPool;
            if (ChangePool(newPool, out previousPool))
            {
                ProjectilePoolsSaver.SavePool(previousPool);
            }
        }

        private void UpgradeStats(float damage, float travelVelocity, float shootDelay, TowerSpecialSO specialSO)
        {
            _damage = damage;
            _travelVelocity = travelVelocity;
            _shootDelay = shootDelay;
            _specialSO = specialSO;
            
            InitSequence();
        }
        
        private bool ChangePool(ProjectilePool projectilePool, out ProjectilePool previousPool)
        {
            previousPool = _projectilePool;
            _projectilePool = projectilePool;

            return previousPool != null;
        }

        public void FocusTarget(Enemy target)
        {
            _target = target;

            if (_target == null)
            {
                _shootSequence.Rewind();
                return;
            }
            
            if (!_shootSequence.IsPlaying())
            {
                _shootSequence.Play();
            }
        }

        private void Shoot()
        {
            AudioController.Instance.Sfx.PlayOneShot(AudioController.Instance.Shoot);
            Projectile projectile = _projectilePool.Get();
            projectile.transform.position = _firePoint.position;
            projectile.gameObject.SetActive(true);
            projectile.ShootSelf(_target, _damage, _travelVelocity, _specialSO);
        }

        private void InitSequence()
        {
            _shootSequence = DOTween.Sequence();

            _shootSequence.AppendCallback(Shoot);
            _shootSequence.AppendInterval(_shootDelay);
            _shootSequence.AppendCallback(() => _shootSequence.Restart());

            _shootSequence.SetAutoKill(false);
            if (_target == null)
            {
                _shootSequence.Pause();
            }
        }
    }
}