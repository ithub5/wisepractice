using System;
using Unity.VisualScripting;
using UnityEngine;

namespace TowersSystem.Core
{
    public class TowerView : MonoBehaviour
    {
        [SerializeField] private GameObject _radius;
        [SerializeField] private SpriteRenderer _radiusRenderer;
        
        public Color CurrentColor { get; private set; }

        public event Action OnSelected;

        private void OnMouseDown()
        {
            OnSelected?.Invoke();
        }

        public void ShowRadius()
        {
            ToggleRadius(true);
        }

        public void HideRadius()
        {
            ToggleRadius(false);
        }

        public void ToggleRadius(bool onOff)
        {
            _radius.SetActive(onOff);
        }

        public void Upgrade(float newRadius)
        {
            _radius.transform.localScale = Vector3.one * newRadius * 2;
        }

        public void ChangeColor(Color newColor)
        {
            _radiusRenderer.color = new Color(newColor.r, newColor.g, newColor.b, _radiusRenderer.color.a);

            CurrentColor = newColor;
        }
    }
}