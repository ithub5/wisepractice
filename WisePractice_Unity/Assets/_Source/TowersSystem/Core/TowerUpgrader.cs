using System;
using TowersSystem.Data.Stats;
using UnityEngine;

namespace TowersSystem.Core
{
    public class TowerUpgrader : MonoBehaviour
    {
        [SerializeField] private TowerUpgradesSO upgradesSO;

        private int _currentLevel;

        public int Cost => _currentLevel < upgradesSO.Prices.Length ? upgradesSO.Prices[_currentLevel] : -1;

        public float Damage { get; private set; }
        public float ProjectileTravelVelocity { get; private set; }
        public float ShootDelay { get; private set; }
        
        public event Action<TowerLevelSO> OnUpgrade;

        public void Upgrade()
        {
            PlayerResources.ChangeEnergy(-Cost);
            OnUpgrade?.Invoke(upgradesSO.TowerLevels[_currentLevel]);
            
            Damage = upgradesSO.TowerLevels[_currentLevel].GeneralStats.Projectile.Damage;
            ProjectileTravelVelocity = upgradesSO.TowerLevels[_currentLevel].GeneralStats.Projectile.TravelVelocity;
            ShootDelay = upgradesSO.TowerLevels[_currentLevel].GeneralStats.ShootDelay;
            
            _currentLevel++;
        }
    }
}