using System;
using Entities;
using TowersSystem.Data.Shooting;
using TowersSystem.Data.Stats;
using TowersSystem.Shooting;
using UnityEngine;

namespace TowersSystem.Core
{
    public class TowerInvoker
    {
        private TowerUpgrader _upgrader;
        
        private TowerPhysics _physics;
        private TowerView _view;
        private TowerShooter _shooter;
        private Transform _projectilesHolder;

        public bool IsPlaceable => _physics.IsPlaceable;

        private static TowerInvoker _currentActiveOnInfo;

        public event Action<TowerUpgrader, TowerInvoker> OnShowInfo;

        public TowerInvoker(TowerUpgrader upgrader, TowerPhysics physics, TowerView view, TowerShooter shooter, Transform projectilesHolder)
        {
            _upgrader = upgrader;
            
            _physics = physics;
            _view = view;
            _shooter = shooter;

            _projectilesHolder = projectilesHolder;
        }

        public void Bind()
        {
            _upgrader.OnUpgrade += Upgrade;
            _physics.OnTargetChange += ChangeTarget;
            _physics.OnPlaceableStateChange += UpdatePlacing;
            _view.OnSelected += ShowInfo;
        }

        private void Upgrade(TowerLevelSO newLevel)
        {
            TowerGeneralStats generalStats = newLevel.GeneralStats;
            ProjectileStats projectileStats = generalStats.Projectile;

            _physics.Upgrade(generalStats.ShootRadius, generalStats.EnemyMaks, generalStats.ObstaclesMask);
            _shooter.Upgrade(_projectilesHolder, projectileStats, generalStats.ShootDelay, newLevel.TowerSpecial);
            _view.Upgrade(generalStats.ShootRadius);
        }

        private void ChangeTarget(Enemy enemy)
        {
            _shooter.FocusTarget(enemy);
        }

        public void StartPlacing()
        {
            _view.ShowRadius();
        }

        public void UpdatePlacing()
        {
            if (!_physics.IsPlaceable && _view.CurrentColor != Color.red)
            {
                _view.ChangeColor(Color.red);
            }
            else if (_physics.IsPlaceable && _view.CurrentColor != Color.green)
            {
                _view.ChangeColor(Color.green);
            }
        }

        public void StopPlacing()
        {
            _view.HideRadius();
            _view.ChangeColor(Color.white);
        }

        public void ShowInfo()
        {
            _currentActiveOnInfo?.HideInfo();
            _currentActiveOnInfo = this;
            
            _view.ShowRadius();
            OnShowInfo?.Invoke(_upgrader, this);
        }

        public void HideInfo()
        {
            _view.HideRadius();
        }
    }
}