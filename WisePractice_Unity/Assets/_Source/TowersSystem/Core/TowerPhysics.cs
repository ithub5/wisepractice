using System;
using System.Collections.Generic;
using Entities;
using UnityEngine;
using Utils;

namespace TowersSystem.Core
{
    [RequireComponent(typeof(CircleCollider2D))]
    public class TowerPhysics : MonoBehaviour
    {
        [SerializeField] private Collider2D towerCollider;
        [SerializeField] private CircleCollider2D fireRangeCollider;
        [field: SerializeField] public Transform FirePoint { get; private set; }

        private LayerMask _enemyMask;
        private LayerMask _obstaclesMask;
        
        private List<Transform> _enemies = new List<Transform>();
        private Enemy _currentTarget;

        private List<Collision2D> _obstacles = new List<Collision2D>();

        public bool IsPlaceable { get; private set; } = true;

        public event Action OnPlaceableStateChange;

        public event Action<Enemy> OnTargetChange;

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (!_enemyMask.Contains(col.gameObject.layer))
            {
                return;
            }
            
            _enemies.Add(col.transform);
            if (_enemies.Count < 1)
            {
                return;
            }

            ChangeTarget(_enemies[0].GetComponent<Enemy>());
        }

        private void OnTriggerExit2D(Collider2D col)
        {
            if (!_enemyMask.Contains(col.gameObject.layer))
            {
                return;
            }
            
            RemoveEnemy(col.transform);
        }

        private void OnCollisionEnter2D(Collision2D col)
        {
            if (!_obstaclesMask.Contains(col.gameObject.layer))
            {
                return;
            }
            
            _obstacles.Add(col);
            if (IsPlaceable)
            {
                IsPlaceable = false;
                OnPlaceableStateChange?.Invoke();
            }
        }
        
        private void OnCollisionExit2D(Collision2D col)
        {
            if (!_obstaclesMask.Contains(col.gameObject.layer))
            {
                return;
            }

            _obstacles.Remove(col);
            if (_obstacles.Count == 0 && !IsPlaceable)
            {
                IsPlaceable = true;
                OnPlaceableStateChange?.Invoke();
            }
        }

        public void Upgrade(float newRadius, LayerMask newEnemyMask, LayerMask newObstaclesMask)
        {
            fireRangeCollider.radius = newRadius;
            _enemyMask = newEnemyMask;
            _obstaclesMask = newObstaclesMask;
        }


        private void RemoveEnemy(Enemy enemy)
        {
            RemoveEnemy(enemy.transform);
        }
        
        private void RemoveEnemy(Transform targetTransform)
        {
            Transform previousTarget = _enemies.Count > 0 ? _enemies[0] : default;

            _enemies.Remove(targetTransform);
            if (_enemies.Count == 0)
            {
                ChangeTarget(null);
                return;
            }

            Transform newTarget = _enemies[0];
            if (newTarget == previousTarget)
            {
                return;
            }
            
            ChangeTarget(_enemies[0].GetComponent<Enemy>());
        }

        private void ChangeTarget(Enemy nextTarget)
        {
            if (_currentTarget != null)
            {
                _currentTarget.OnDeath -= RemoveEnemy;
            }
            
            _currentTarget = nextTarget;
            
            if (_currentTarget != null)
            {
                _currentTarget.OnDeath += RemoveEnemy;
            }
            
            OnTargetChange?.Invoke(_currentTarget);
        }
    }
}