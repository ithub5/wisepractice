using DG.Tweening;
using Entities;
using TowersSystem.Data.Stats.Specials;
using Unity.VisualScripting;
using UnityEngine;
using Sequence = DG.Tweening.Sequence;

namespace TowersSystem.SpecialStates
{
    public class PoisonState
    {
        private TowerPoisonSO _poisonSO;

        private float _stateCounter;
        private Sequence _sequence;

        private Enemy _target;
        private SpriteRenderer _renderer;

        private Color _normalColor;

        public PoisonState(Enemy target, SpriteRenderer renderer)
        {
            _target = target;
            _renderer = renderer;

            _target.OnDeath += Stop;
        }
        
        public void Set(TowerPoisonSO poison)
        {
            if (_stateCounter > 0)
            {
                return;
            }

            if (poison == _poisonSO)
            {
                _stateCounter = _poisonSO.Duration;
                if (_sequence.IsPlaying())
                {
                    return;
                }
                
                _sequence.Restart();
            }
            
            _poisonSO = poison;
            SetSequence();
        }

        private void SetSequence()
        {
            _sequence = DOTween.Sequence();

            _sequence.SetAutoKill(false);
            
            _stateCounter = _poisonSO.Duration;

            _normalColor = _renderer.color;
            Color poisonColor = new Color(_poisonSO.Color.r, _poisonSO.Color.g, _poisonSO.Color.b, _renderer.color.a);
            _renderer.color = poisonColor;
            
            _sequence.AppendCallback(() => { if (_stateCounter <= 0) { _sequence.Pause(); _renderer.color = _normalColor; } });
            _sequence.AppendCallback(() => _target.TakeDamage(_poisonSO.Damage));
            _sequence.AppendInterval(_poisonSO.CycleTime);
            _sequence.AppendCallback(() => _stateCounter -= _poisonSO.CycleTime);
            _sequence.AppendCallback(() => _sequence.Restart());
        }

        private void Stop(Enemy enemy)
        {
            _sequence.Pause();
        }
    }
}