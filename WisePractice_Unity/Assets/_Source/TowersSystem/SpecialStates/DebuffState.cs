using System;
using DG.Tweening;
using Entities;
using TowersSystem.Data.Stats.Specials;
using TowersSystem.Utils;
using UnityEngine;

namespace TowersSystem.SpecialStates
{
    public class DebuffState
    {
        private TowerDebuffSO _debuffSO;

        private float _stateCounter;
        private Sequence _sequence;

        private Enemy _target;
        private SpriteRenderer _renderer;

        private Color _normalColor;
        
        private Action<float> _debuffApplier;

        public DebuffState(Enemy target, SpriteRenderer renderer)
        {
            _target = target;
            _renderer = renderer;

            _target.OnDeath += Stop;
        }

        public void Set(TowerDebuffSO debuff)
        {
            if (_stateCounter > 0)
            {
                return;
            }

            if (debuff == _debuffSO)
            {
                _stateCounter = _debuffSO.Duration;
                if (_sequence.IsPlaying())
                {
                    return;
                }
                
                _sequence.Restart();
            }
            
            _debuffSO = debuff;
            SetDebuffApplier();
            SetSequence();
        }
        
        private void SetDebuffApplier()
        {
            switch (_debuffSO.Type)
            {
                case DebuffType.deceleration:
                    _debuffApplier = (value) => _target.SpeedMultiplier = value;
                    break;
            }
        }

        private void SetSequence()
        {
            _sequence = DOTween.Sequence();

            _sequence.SetAutoKill(false);
            
            _stateCounter = _debuffSO.Duration;

            _normalColor = _renderer.color;
            Color debuffColor = new Color(_debuffSO.Color.r, _debuffSO.Color.g, _debuffSO.Color.b, _renderer.color.a); 
            

            _sequence.AppendCallback(() => _debuffApplier(_debuffSO.Value));
            _sequence.AppendCallback(() => _renderer.color = debuffColor);
            _sequence.AppendInterval(_debuffSO.Duration);
            _sequence.AppendCallback(() => _debuffApplier(1f));
            _sequence.AppendCallback(() => _renderer.color = _normalColor);
        }
        
        
        private void Stop(Enemy obj)
        {
            _sequence.Pause();
        }
    }
}