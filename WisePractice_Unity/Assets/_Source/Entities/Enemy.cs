using System;
using System.Collections;
using System.Collections.Generic;
using Battle;
using Entities.Data;
using TMPro;
using TowersSystem.Data.Stats.Specials;
using TowersSystem.SpecialStates;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace Entities
{
    public class Enemy : MonoBehaviour
    {
        [SerializeField] private EnemySettingsSO enemySettingsSO;
        [SerializeField] private GameObject deathText;
        [SerializeField] private bool notScalable;

        [SerializeField] private SpriteRenderer spriteRenderer;

        private static readonly int Dead = Animator.StringToHash("Dead");
        private static readonly int Damaged = Animator.StringToHash("Damaged");
        
        private List<GameObject> _enemyWaypoints;
        private int _passedWaypoints;
        private Vector2 _target;
        private float _health, _reward;
        private bool _isAlive = true;
        private Slider _healthSlider;
        private Animator _animator;
        private TMP_Text _deathTMPText;
        
        private PoisonState _poisonState;
        private DebuffState _debuffState;
        
        

        public float SpeedMultiplier { get; set; } = 1f;
        
        public event Action<Enemy> OnDeath;
        public static event Action OnEnemyDeath;
        
        private void Awake()
        {
            _animator = GetComponent<Animator>();
            _healthSlider = GetComponentInChildren<Slider>();
            _deathTMPText = deathText.GetComponentInChildren<TMP_Text>();
            _enemyWaypoints = WaveController.EnemyWaypoints;

            _poisonState = new PoisonState(this, spriteRenderer);
            _debuffState = new DebuffState(this, spriteRenderer);
            
            if (!notScalable)
            {
                _health = enemySettingsSO.Health * Mathf.Pow(WaveController.WaveEnemiesHealthMultiplier, WaveController.WaveNumber-1);
                _reward = enemySettingsSO.Reward * Mathf.Pow(WaveController.WaveEnemiesRewardMultiplier, WaveController.WaveNumber-1);
            }
            else
            {
                _health = enemySettingsSO.Health;
                _reward = enemySettingsSO.Reward;
            }
        }

        private void Start()
        {
            _target = _enemyWaypoints[1].transform.position;

            _healthSlider.maxValue = _health;
            _healthSlider.value = _health;
            _healthSlider.gameObject.SetActive(false);
        }
        
        private void Update()
        {
            if (!_isAlive) return;
            
            transform.position =
                Vector2.MoveTowards(transform.position, _target, enemySettingsSO.Speed * SpeedMultiplier * Time.deltaTime);

            if (transform.position.Equals(_target))
            {
                _passedWaypoints++;
                _target = _enemyWaypoints[_passedWaypoints].transform.position;
            }
            
        }

        private void OnTriggerEnter2D(Collider2D col)
        {
            if (col.CompareTag("Player"))
            {
                PlayerBase.TakeDamage(enemySettingsSO.Damage);
                Death();
            }
        }

        public void TakeDamage(float value)
        {
            if (!_isAlive) return;
            
            _health -= value;
            _animator.SetBool(Damaged,true);
            
            HealthSliderValueChange();
            if (_health <= 0)
            {
                Death();
            }
        }

        private void HealthSliderValueChange()
        {
            if (!_healthSlider.IsActive() && _isAlive)
            {
                _healthSlider.gameObject.SetActive(true);
            }
            _healthSlider.value = _health;
        }

        private IEnumerator BeforeEnemyDestroy()
        {
            _animator.SetTrigger(Dead);
            _healthSlider.gameObject.SetActive(false);
            
            yield return new WaitForSeconds(0.6f);
            _deathTMPText.text = $"+{_reward:0} energy";
            var rewardText = Instantiate(deathText, transform.position, quaternion.identity,transform.parent.GetChild(0));
            PlayerResources.ChangeEnergy(_reward);
            
            yield return new WaitForSeconds(3f);
            Destroy(rewardText);
            Destroy(gameObject);
        }
        private void Death()
        {
            _isAlive = false;
            OnDeath?.Invoke(this);
            OnEnemyDeath?.Invoke();
            StartCoroutine(BeforeEnemyDestroy());
        }

        public void ApplyPoison(TowerPoisonSO poisonSO)
        {
            _poisonState.Set(poisonSO);
        }

        public void ApplyDebuff(TowerDebuffSO debuffSO)
        {
            _debuffState.Set(debuffSO);
        }
    }
}