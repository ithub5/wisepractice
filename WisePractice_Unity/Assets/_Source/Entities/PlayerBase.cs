using System;
using Audio;
using Entities.Data;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Entities
{
    public class PlayerBase : MonoBehaviour
    {
        [SerializeField] private PlayerSettingsSO playerSettingsSO;
        public static event Action OnPlayerHealthChange;
        public static event Action OnPlayerDeath;
        public static float Heath { get; private set; }

        private void Awake()
        {
            PlayerResources.ResetEnergy();
            PlayerResources.ChangeEnergy(playerSettingsSO.StartEnergy);
            Heath = playerSettingsSO.Heath;
        }

        public static void TakeDamage(float value)
        {
            Heath -= value;
            if (Heath <= 0)
            {
                Death();
            }
            
            OnPlayerHealthChange?.Invoke();
        }

        private static void Death()
        {
            OnPlayerDeath?.Invoke();
            ButtonMethods.LoadCurrentScene();
        }
    }
}
