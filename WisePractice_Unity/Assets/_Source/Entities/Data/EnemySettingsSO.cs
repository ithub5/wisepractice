using UnityEngine;

namespace Entities.Data
{
    [CreateAssetMenu(fileName = "EnemySettings", menuName = "SO/EnemySettings")]
    public class EnemySettingsSO : ScriptableObject
    {
        [field: SerializeField] public float Health { get; private set; }
        [field: SerializeField] public float Speed { get; private set; }
        [field: SerializeField] public float Reward { get; private set;  }
        [field: SerializeField] public float Damage { get; private set;  }
    }
}
