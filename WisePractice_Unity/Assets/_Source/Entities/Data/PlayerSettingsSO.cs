using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Entities.Data
{
    [CreateAssetMenu(fileName = "PlayerSettings", menuName = "SO/PlayerSettings")]
    public class PlayerSettingsSO : ScriptableObject
    {
        [field:SerializeField] public float Heath { get; private set; }
        [field:SerializeField] public float StartEnergy { get; private set; }
    }
}