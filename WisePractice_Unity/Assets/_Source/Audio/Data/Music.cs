using System;
using UnityEngine;

namespace Audio.Data
{
    [Serializable]
    public class Music
    {
        [field: SerializeField] public AudioClip Menu { get; private set; }
        [field: SerializeField] public AudioClip Battle { get; private set; }
        [field: SerializeField] public AudioClip Win { get; private set; }
        [field: SerializeField] public AudioClip Lose { get; private set;}
    }
}
