using System;
using Audio.Data;
using Battle;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Audio
{
    public class AudioController : MonoBehaviour
    {
        [field:SerializeField] public AudioSource Background { get; private set; }
        [field:SerializeField] public AudioSource Sfx { get; private set; }
        [field:SerializeField] public Music BackgroundMusic { get; private set; }
        [field:SerializeField] public AudioClip Shoot { get; private set; }
        public static AudioController Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }
        
        public void ChangeBackgroundMusic(AudioClip bgmusic)
        {
            Background.clip = bgmusic;
            Background.Play();
        }
    }
}
