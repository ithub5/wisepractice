using System;
using UnityEngine;
using UnityEngine.UI;

namespace Audio
{
    public class AudioVolume : MonoBehaviour
    {
        [SerializeField] private Slider backgroundVolumeSlider, sfxVolumeSlider;

        private void Start()
        {
            if (PlayerPrefs.HasKey("bgvolume"))
            {
                backgroundVolumeSlider.value = PlayerPrefs.GetFloat("bgvolume");
                BackgroundVolumeChange();
            }
            if (PlayerPrefs.HasKey("sfxvolume"))
            {
                sfxVolumeSlider.value = PlayerPrefs.GetFloat("sfxvolume");
                SfxVolumeChange();
            }
            AudioController.Instance.ChangeBackgroundMusic(AudioController.Instance.BackgroundMusic.Menu);
        }

        public void BackgroundVolumeChange()
        {
            AudioController.Instance.Background.volume = backgroundVolumeSlider.value;
            PlayerPrefs.SetFloat("bgvolume", backgroundVolumeSlider.value);
        }
        public void SfxVolumeChange()
        {
            AudioController.Instance.Sfx.volume = sfxVolumeSlider.value;
            PlayerPrefs.SetFloat("sfxvolume", sfxVolumeSlider.value);
        }
    }
}
