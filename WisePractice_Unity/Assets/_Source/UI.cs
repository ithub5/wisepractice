using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using Battle;
using Core;
using Entities;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    [SerializeField] private TMP_Text energy, wave, waveTimer;
    [SerializeField] private Slider playerHealthSlider;
    [SerializeField] private Button nextWaveButton;
    [SerializeField] private GameObject winScreen;

    private void Awake()
    {
        PlayerResources.OnEnergyChange += EnergyTextChange;
        WaveController.OnWaveStart += WaveTextChange;
        WaveController.OnWaveStart += NextWaveButtonSetInteractableFalse;
        WaveController.OnAllEnemiesOnWaveDeath += NextWaveButtonSetInteractableTrue;
        WaveController.OnLastEnemyDeath += EnableWinScreen;
        PlayerBase.OnPlayerHealthChange += PlayerHealthUIChange;
    }

    private void Start()
    {
        playerHealthSlider.maxValue = PlayerBase.Heath;
        
        EnergyTextChange();
        WaveTextChange();
        PlayerHealthUIChange();
    }

    private void Update()
    {
        WaveTimerTextChange();
    }

    private void OnDisable()
    {
        PlayerResources.OnEnergyChange -= EnergyTextChange;
        WaveController.OnWaveStart -= WaveTextChange;
        WaveController.OnWaveStart -= NextWaveButtonSetInteractableFalse;
        WaveController.OnAllEnemiesOnWaveDeath -= NextWaveButtonSetInteractableTrue;
        WaveController.OnLastEnemyDeath -= EnableWinScreen;
        PlayerBase.OnPlayerHealthChange -= PlayerHealthUIChange;
    }

    private void EnergyTextChange()
    {
        energy.text = $"Energy: {PlayerResources.Energy:0}";
    }
    private void WaveTextChange()
    {
        wave.text = $"Wave: {WaveController.WaveNumber}/{WaveController.WaveTotalNumber}";
    }
    private void PlayerHealthUIChange()
    {
        playerHealthSlider.value = PlayerBase.Heath;
    }

    private void WaveTimerTextChange()
    {
        if (WaveController.WaveTimer > 0)
        {
            waveTimer.text = !WaveController.WavesEnded ? $"Next wave in {WaveController.WaveTimer:0.0}s" : "Last wave"; 
        }
        else
        {
            waveTimer.text = !WaveController.WavesEnded ? $"Next wave in {WaveController.WaveTimer:0}s" : "Last wave"; 
        }
    }

    private void NextWaveButtonSetInteractableTrue()
    {
        nextWaveButton.interactable = true;
    }
    private void NextWaveButtonSetInteractableFalse()
    {
        nextWaveButton.interactable = false;
    }

    private void EnableWinScreen()
    {
        winScreen.SetActive(true);
    }
}
