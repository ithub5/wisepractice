using System;
using System.Collections;
using System.Collections.Generic;
using Audio;
using Battle.Data;
using Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Battle
{
    public class WaveController : MonoBehaviour
    {
        [SerializeField] private Transform enemyParent;

        [SerializeField, Header("Маршрут врагов")]
        private List<GameObject> enemyWaypoints;

        [SerializeField, Header("Пресет волны")]
        private WavePresetSO wavePresetSO;

        private bool _isWavesStarted;
        private int _enemiesNeedToKill, _enemiesNeedToSpawn, _enemiesSpawned, _uniqueEnemiesSpawned;
        private Wave _currentWave;
        
        public static event Action OnWaveStart;
        public static event Action OnAllEnemiesOnWaveDeath;
        public static event Action OnLastEnemyDeath;

        public static List<GameObject> EnemyWaypoints { get; private set; }
        public static int WaveTotalNumber { get; private set; }
        public static int WaveNumber { get; private set; }
        public static float WaveTimer { get; private set; }
        public static float WaveEnemiesHealthMultiplier { get; private set; }
        public static float WaveEnemiesRewardMultiplier { get; private set; }
        public static bool WavesEnded { get; private set; }
        public static int DeadEnemies { get; private set; }
        
        private void Awake()
        {
            Enemy.OnEnemyDeath += ChangeDeadEnemiesCount;
        }

        private void Start()
        {
            AudioController.Instance.Background.clip = null;
            EnemyWaypoints = enemyWaypoints;
            WaveTotalNumber = wavePresetSO.Waves.Count;
            WaveEnemiesHealthMultiplier = wavePresetSO.WaveHpMultiplier;
            WaveEnemiesRewardMultiplier = wavePresetSO.WaveRewardMultiplier;
            WaveNumber = 0;
            WaveTimer = 0;
            DeadEnemies = 0;
            WavesEnded = false;
        }

        private void Update()
        {
            if (!_isWavesStarted)
            {
                return;
            }
            WaveTimer -= Time.deltaTime;
        }

        private void OnDisable()
        {
            Enemy.OnEnemyDeath -= ChangeDeadEnemiesCount;
        }

        public void StartCombat()
        {
            StartCoroutine(StartWave());
        }

        private IEnumerator StartEnemiesSpawning()
        {
            while (true)
            {
                if (_enemiesNeedToSpawn == 0)
                {
                    yield break;
                }

                SpawnEnemy();

                if (wavePresetSO.Waves[WaveNumber - 1].EnemyCount[_enemiesSpawned - 1].DelayAfterSpawn == 0)
                {
                    yield return new WaitForSeconds(wavePresetSO.Waves[WaveNumber - 1].DefaultEnemySpawnDelay);
                }
                else
                {
                    yield return new WaitForSeconds(wavePresetSO.Waves[WaveNumber - 1].EnemyCount[_enemiesSpawned - 1]
                        .DelayAfterSpawn);
                }
            }
        }

        private IEnumerator StartWave()
        {
            while (true)
            {
                if (!_isWavesStarted)
                {
                    _isWavesStarted = true;
                    AudioController.Instance.ChangeBackgroundMusic(AudioController.Instance.BackgroundMusic.Battle);
                }
                
                WaveNumber++;
                _enemiesNeedToSpawn = wavePresetSO.Waves[WaveNumber - 1].EnemyCount.Count;
                _enemiesNeedToKill += _enemiesNeedToSpawn;
                _enemiesSpawned = 0;

                StartCoroutine(StartEnemiesSpawning());
                OnWaveStart?.Invoke();

                if (WaveNumber == wavePresetSO.Waves.Count)
                {
                    WavesEnded = true;
                    Debug.Log("AllWavesSpawned");
                    yield break;
                }

                if (wavePresetSO.Waves[WaveNumber - 1].CustomDelayBetweenNextWave == 0)
                {
                    WaveTimer = wavePresetSO.WaveDelay;
                    yield return new WaitForSeconds(wavePresetSO.WaveDelay);
                }
                else
                {
                    WaveTimer = wavePresetSO.Waves[WaveNumber - 1].CustomDelayBetweenNextWave;
                    yield return new WaitForSeconds(wavePresetSO.Waves[WaveNumber - 1].CustomDelayBetweenNextWave);
                }
            }
        }

        private void SpawnEnemy()
        {
            switch (wavePresetSO.Waves[WaveNumber - 1].EnemyCount[_enemiesSpawned].EnemyType)
            {
                case EnemyTypes.Regular:
                    Instantiate(wavePresetSO.EnemyPrefabs.RegularEnemy, enemyWaypoints[0].transform.position, quaternion.identity, enemyParent);
                    break;
                case EnemyTypes.Fast:
                    Instantiate(wavePresetSO.EnemyPrefabs.FastEnemy, enemyWaypoints[0].transform.position, quaternion.identity, enemyParent);
                    break;
                case EnemyTypes.Slow:
                    Instantiate(wavePresetSO.EnemyPrefabs.SlowEnemy, enemyWaypoints[0].transform.position, quaternion.identity, enemyParent);
                    break;
                case EnemyTypes.Unique:
                    Instantiate(wavePresetSO.UniqueEnemyPrefabs[_uniqueEnemiesSpawned], enemyWaypoints[0].transform.position, quaternion.identity, enemyParent);
                    _uniqueEnemiesSpawned++;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            _enemiesSpawned++;
            _enemiesNeedToSpawn--;
        }

        private void ChangeDeadEnemiesCount()
        {
            DeadEnemies++;
            if (DeadEnemies >= _enemiesNeedToKill)
            {
                OnAllEnemiesOnWaveDeath?.Invoke();
                if (WaveNumber == wavePresetSO.Waves.Count)
                {
                    OnLastEnemyDeath?.Invoke();
                    AudioController.Instance.ChangeBackgroundMusic(AudioController.Instance.BackgroundMusic.Win);
                }
            }
        }
    }
}