using System;
using UnityEngine;

namespace Battle.Data
{
    [Serializable]
    public enum EnemyTypes
    {
        Regular,
        Fast,
        Slow,
        Unique
    }
    
    [Serializable]
    public class DefaultEnemyTypes
    {
        [field: SerializeField] public GameObject RegularEnemy { get; private set; }
        [field: SerializeField] public GameObject FastEnemy { get; private set;}
        [field: SerializeField] public GameObject SlowEnemy { get; private set;}
    }
}