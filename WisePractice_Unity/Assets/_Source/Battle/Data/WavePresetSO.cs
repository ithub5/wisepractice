using System;
using System.Collections.Generic;
using UnityEngine;

namespace Battle.Data
{
    [CreateAssetMenu(fileName = "WavePreset", menuName = "SO/WavePreset")]
    public class WavePresetSO : ScriptableObject
    {
        [field:SerializeField, Header("Префабы обычных врагов")] public DefaultEnemyTypes EnemyPrefabs { get; private set; }
        [field:SerializeField, Header("Префабы уникальных врагов"), Tooltip("Префабы уникальных врагов по порядку спавна, 1 префаб = 1 заспавненный unique enemy")] public GameObject[] UniqueEnemyPrefabs { get; private set; }
        [field:SerializeField, Header("Количество волн")] public List<Wave> Waves { get; private set; }
        [field:SerializeField, Header("Базовое время между волнами")] public float WaveDelay { get; private set; }

        [field: SerializeField, Header("Множители параметров врагов за волну"), Tooltip("Число, на которое умножается хп врагов за каждую волну после 1-й")]
        public float WaveHpMultiplier { get; private set; } = 1;

        [field: SerializeField, Tooltip("Число, на которое умножается награда врагов за каждую волну после 1-й")]
        public float WaveRewardMultiplier { get; private set; } = 1;
    }
}