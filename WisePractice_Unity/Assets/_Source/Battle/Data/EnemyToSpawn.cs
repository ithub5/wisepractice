using System;
using UnityEngine;

namespace Battle.Data
{
    [Serializable]
    public class EnemyToSpawn
    {
        [field:SerializeField, Header("Тип врага")] public EnemyTypes EnemyType { get; private set; }
        [field:SerializeField, Header("Кастомное время перед спавном следующего врага"), Tooltip("Настраиваемое время перед спавном следующего врага, 0 = дефолтное")] public float DelayAfterSpawn { get; private set; }
    }
}
