using System;
using System.Collections.Generic;
using UnityEngine;

namespace Battle.Data
{
    [Serializable]
    public class Wave
    {
        [field:SerializeField, Header("Кастомное время перед следующей волной"), Tooltip("Настраиваемое время перед спавном следующей волны, 0 = дефолтное")] public float CustomDelayBetweenNextWave { get; private set; }
        [field:SerializeField, Header("Базовое время между спавном врагов")] public float DefaultEnemySpawnDelay { get; private set; }
        [field:SerializeField, Header("Количество врагов")] public List<EnemyToSpawn> EnemyCount { get; private set; }
    }
}