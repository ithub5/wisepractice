using System.Collections.Generic;
using TowersSystem.Data;
using TowersSystem.Data.Stats;
using TowerSystem.Types;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;

namespace TowerSystem.CustomEditors
{
    [CustomEditor(typeof(TowerUpgradesSO))]
    public class TowerUpgradesEditor : Editor
    {
        private TowerUpgradesSO _castedTarget;
            
        private SerializedProperty _levelsStats;
        private SerializedProperty _levelPrices;
        
        private List<TowerLevelEditorHolder> _levelEditors;

        private List<TowerLevelEditor> _currentTowerLevelEditors;

        private void OnEnable()
        {
            _castedTarget = target as TowerUpgradesSO;
            
            _levelEditors = new List<TowerLevelEditorHolder>();
            _currentTowerLevelEditors = new List<TowerLevelEditor>();

            SerializedProperty iterator = serializedObject.GetIterator();
            iterator.NextVisible(true);
            iterator.NextVisible(false);
            _levelsStats = iterator.Copy();
            iterator.NextVisible(false);
            _levelPrices = iterator.Copy();
            
            for (int i = 0; i < _levelsStats.arraySize; i++)
            {
                _levelEditors.Add(new TowerLevelEditorHolder(null));
                _currentTowerLevelEditors.Add(null);
                CheckTowerLevel(i);
            }
        }

        public override void OnInspectorGUI()
        {
            for (int i = 0; i < _levelsStats.arraySize; i++)
            {
                EditorGUILayout.PropertyField(_levelsStats.GetArrayElementAtIndex(i), new GUIContent($"{i + 1} Level"));
                EditorGUILayout.PropertyField(_levelPrices.GetArrayElementAtIndex(i), new GUIContent("Price"));
                serializedObject.ApplyModifiedProperties();
                
                if (CheckTowerLevel(i))
                {
                    DrawLevel(i);
                }
                GUILayout.Space(10f);
            }
            
            GUILayout.Space(20f);
            
            if (GUILayout.Button("Add Level"))
            {
                AddLevel();
            }

            if (GUILayout.Button("Clear"))
            {
                _levelsStats.ClearArray();
            }
            
            serializedObject.ApplyModifiedProperties();
        }

        private bool CheckTowerLevel(int index)
        {
            if (_castedTarget.TowerLevels[index] == null)
            {
                _currentTowerLevelEditors[index] = null;
                return false;
            }

            if (_levelEditors[index].Editor == null || _currentTowerLevelEditors[index] != _levelEditors[index].Editor)
            {
                _levelEditors[index].Editor =
                    CreateEditor(_levelsStats.GetArrayElementAtIndex(index).objectReferenceValue) as TowerLevelEditor;
                _currentTowerLevelEditors[index] = _levelEditors[index].Editor;
            }

            return true;
        }

        private void AddLevel()
        {
            _levelsStats.arraySize++;
            _levelPrices.arraySize++;

            if (_levelEditors.Count < _levelsStats.arraySize)
            {
                _levelEditors.Add(new TowerLevelEditorHolder(null));
                _currentTowerLevelEditors.Add(null);
            }

            serializedObject.ApplyModifiedProperties();
        }

        private void RemoveLevel(int index)
        {
            _levelsStats.DeleteArrayElementAtIndex(index);
            _levelPrices.DeleteArrayElementAtIndex(index);
            
            _levelEditors[index] = new TowerLevelEditorHolder(null);
            _currentTowerLevelEditors[index] = null;
        }

        private void MoveLevelUp(int index)
        {
            if (index == 0)
            {
                return;
            }
            
            MoveLevel(index, true);
        }

        private void MoveLevelDown(int index)
        {
            if (index == _levelsStats.arraySize - 1)
            {
                return;
            }
            
            MoveLevel(index, false);
        }

        private void MoveLevel(int index, bool upDown)
        {
            int direction = upDown ? -1 : 1; 
            
            _levelsStats.MoveArrayElement(index + direction, index);
            
            (_levelEditors[index], _levelEditors[index + direction]) =
                (_levelEditors[index + direction], _levelEditors[index]);
            
            serializedObject.ApplyModifiedProperties();
        }

        private void DrawLevel(int index)
        {
            _levelEditors[index].DoDraw = EditorGUILayout.BeginFoldoutHeaderGroup(_levelEditors[index].DoDraw,
                $"Stats");
            
            if (_levelEditors[index].DoDraw)
            {
                _levelEditors[index].Editor.Draw();
            }
            
            GUILayout.Space(5f);
            
            if (GUILayout.Button("Remove Level"))
            {
                RemoveLevel(index);
            }

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Move Up"))
            {
                MoveLevelUp(index);
            }

            if (GUILayout.Button("Move Down"))
            {
                MoveLevelDown(index);
            }
            EditorGUILayout.EndHorizontal();
            
            EditorGUILayout.EndFoldoutHeaderGroup();
        }
    }
}