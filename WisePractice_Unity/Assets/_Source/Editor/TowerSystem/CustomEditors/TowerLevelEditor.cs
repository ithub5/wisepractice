using System.Collections.Generic;
using TowersSystem.Data.Stats;
using TowersSystem.Data.Stats.Specials;
using UnityEditor;
using UnityEngine;

namespace TowerSystem.CustomEditors
{
    [CustomEditor(typeof(TowerLevelSO))]
    public class TowerLevelEditor : Editor
    {
        private TowerLevelSO _castedTarget;
        
        private SerializedProperty _common;
        private SerializedProperty _special;
        
        private TowerSpecialSO _currentSpecial;
        private SerializedObject _specialSerializedObject;
        private List<SerializedProperty> _specialProperties;

        private bool _foldoutSpecialProperties;

        private void OnEnable()
        {
            _castedTarget = target as TowerLevelSO;
            
            SerializedProperty properties = serializedObject.GetIterator();
            properties.NextVisible(true);

            properties.NextVisible(false);
            _common = properties.Copy();
            properties.NextVisible(false);
            _special = properties.Copy();
            
            _specialProperties = new List<SerializedProperty>();
            _currentSpecial = null;
        }

        public override void OnInspectorGUI()
        {
            Draw();
        }

        public void Draw()
        {
            EditorGUILayout.PropertyField(_common);
            
            EditorGUILayout.Space(10f);
            EditorGUILayout.PropertyField(_special);
            
            serializedObject.ApplyModifiedProperties();

            if (!CheckNonCommon())
            {
                return;
            }

            _foldoutSpecialProperties = EditorGUILayout.Foldout(_foldoutSpecialProperties, nameof(_castedTarget.TowerSpecial), true);
            if (_foldoutSpecialProperties)
            {
                DrawNonCommon();
            }
            
            serializedObject.ApplyModifiedProperties();
        }
        
        private bool CheckNonCommon()
        {
            if (_castedTarget.TowerSpecial == null)
            {
                _currentSpecial = null;
                return false;
            }
            
            if (_currentSpecial != _castedTarget.TowerSpecial)
            {
                FillNonCommonProperties();
                _currentSpecial = _castedTarget.TowerSpecial;
            }

            return true;
        }

        private void FillNonCommonProperties()
        {
            _specialProperties.Clear();

            _specialSerializedObject = new SerializedObject(_special.objectReferenceValue);
            SerializedProperty nonCommonSerializedProperties = _specialSerializedObject.GetIterator();
            nonCommonSerializedProperties.NextVisible(true);

            while (nonCommonSerializedProperties.NextVisible(false))
            {
                _specialProperties.Add(nonCommonSerializedProperties.Copy());
            }
        }

        private void DrawNonCommon()
        {
            foreach (var property in _specialProperties)
            {
                EditorGUILayout.PropertyField(property);
            }
            
            _specialSerializedObject.ApplyModifiedProperties();
        }
    }
}