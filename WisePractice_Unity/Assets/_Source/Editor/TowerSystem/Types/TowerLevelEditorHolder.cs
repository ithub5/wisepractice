using TowerSystem.CustomEditors;
using UnityEngine;

namespace TowerSystem.Types
{
    public class TowerLevelEditorHolder
    {
        public TowerLevelEditor Editor { get; set; }
        public bool DoDraw { get; set; }

        public TowerLevelEditorHolder(TowerLevelEditor editor, bool doDraw = false)
        {
            Editor = editor;
            DoDraw = doDraw;
        }
    }
}