using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonMethods : MonoBehaviour
{
    public static void LoadCurrentScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public static void LoadScene(int index)
    {
        SceneManager.LoadScene(index);
    }
    public static void SetReverse(GameObject gameObject)
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }
}
